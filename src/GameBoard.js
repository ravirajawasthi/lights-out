import React, { Component } from 'react';
import Tile from './Tile';
import uuid from 'uuid';
import './GameBoard.css';

class GameBoard extends Component {
  static defaultProps = {
    n: 5
  };
  constructor(props) {
    super(props);
    let mat = new Array(this.props.n).fill(0);
    mat.forEach((el, row) => {
      mat[row] = new Array(this.props.n).fill(0);
      mat[row].forEach((el, col) => {
        mat[row][col] = Math.round(Math.random()) === 1 ? true : false;
      });
    });
    this.state = {
      gameOver: false,
      board: mat
    };
    this.chooseTile = this.chooseTile.bind(this);
    this.checkWin = this.checkWin.bind(this);
  }

  chooseTile(pos) {
    let x = pos[0];
    let y = pos[1];
    let newBoard = this.state.board.slice();
    newBoard[x][y] = !newBoard[x][y];
    if (x + 1 < this.props.n && y < this.props.n) {
      //x + 1, y
      newBoard[x + 1][y] = !newBoard[x + 1][y];
    }
    if (x < this.props.n && y + 1 < this.props.n) {
      //x, y + 1
      newBoard[x][y + 1] = !newBoard[x][y + 1];
    }
    if (x - 1 >= 0 && y < this.props.n) {
      //x - 1 , y
      newBoard[x - 1][y] = !newBoard[x - 1][y];
    }
    if (x < this.props.n && y - 1 >= 0) {
      //x, y - 1
      newBoard[x][y - 1] = !newBoard[x][y - 1];
    }
    let win = this.checkWin();
    this.setState({ win: win, board: newBoard });
  }

  checkWin() {
    for (let i = 0; i < this.props.n; i += 1) {
      for (let j = 0; j < this.props.n; j += 1) {
        console.log(i, j);
        if (this.state.board[i][j] === true) {
          return false;
        }
      }
    }
    return true;
  }

  render() {
    let tiles = [];
    if (!this.state.win) {
      for (let i = 0; i < this.props.n; i += 1) {
        for (let j = 0; j < this.props.n; j += 1) {
          tiles.push(
            <Tile
              size={this.props.n}
              position={[i, j]}
              lit={this.state.board[i][j]}
              key={uuid()}
              chooseTile={this.chooseTile}
            />
          );
        }
      }
    }
    return (
      <div className="GameBoard">
        {this.state.win ? (
          <div className="GameBoard-winningMessage">
            <div className="neon">Chop Chop</div>
            <div className="flux">You win</div>
          </div>
        ) : (
          <div>
            <div className="GameBoard-header">
              <div className="neon">Lights</div>
              <div className="flux">Out</div>
            </div>
            <div className="GameBoard-board">{tiles}</div>
          </div>
        )}
      </div>
    );
  }
}

export default GameBoard;

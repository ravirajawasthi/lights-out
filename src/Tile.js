import React, { Component } from 'react';
import './Tile.css';
class Tile extends Component {
  constructor(props) {
    super(props);
    this.handleTile = this.handleTile.bind(this);
  }
  handleTile() {
    this.props.chooseTile(this.props.position);
  }
  render() {
    return (
      <div
        className={`Tile ${this.props.lit ? 'Tile-lit' : 'Tile-nlit'}`}
        onClick={() => {
          this.props.chooseTile(this.props.position);
        }}
      ></div>
    );
  }
}

export default Tile;
